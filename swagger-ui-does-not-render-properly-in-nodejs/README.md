
# #1 EQA

# Running Project
- **Requirements**:
    - nodejs

**Installing**
```zsh
npm install
```

**Running**
```zsh
npm start
```

**Endpoints Access**:
```
/api-docs -> swagger ui api documentation
/api/user -> user endpoint (only GET implemented)
```

**Observations**:
Port is setup to 3000, you may change at /bin/www <br>
Or you may change port process.env.PORT when running project, as it follows:
```zsh
PORT=3003 npm start
```

# References and Resources
- #1 EQA [project code](https://gitlab.com/sistemaon/stackoverflow-eqa/-/tree/master/swagger-ui-does-not-render-properly-in-nodejs) and [stackoverflow question](https://stackoverflow.com/questions/63028216/swagger-ui-does-not-render-properly-in-nodejs)
    - Project name inside: _swagger-ui-does-not-render-properly-in-nodejs_

    - resources/references:
        - https://www.npmjs.com/package/swagger-jsdoc
        - https://github.com/Surnet/swagger-jsdoc/blob/HEAD/docs/GETTING-STARTED.md
        - https://www.npmjs.com/package/swagger-ui-express/
        - https://swagger.io/docs/specification/about/
        
        - Swagger V2:
            - https://swagger.io/specification/v2/
            - https://swagger.io/docs/specification/2-0/basic-structure/

        - Swagger V3:
            - https://swagger.io/docs/specification/components/
            - https://swagger.io/docs/specification/using-ref/
            - https://swagger.io/docs/specification/callbacks/
            - https://swagger.io/docs/specification/basic-structure/

### Swagger ui endpoint
![swagger ui endpoint](../imgs/00.png)

### User execute get
![user execute get](../imgs/01.png)

### Models schemas
![models schemas](../imgs/02.png)