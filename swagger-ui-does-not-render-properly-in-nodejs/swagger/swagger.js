'use strict';

module.exports = {
  swaggerDefinition: {
    swagger: '2.0',
    info: {
      title: 'test API',
      version: '1.0.0',
      description: 'test server endpoints'
    },
    basePath: '/api',
    schemes: ['http', 'https'],
    securityDefinitions: {
      Bearer: {
        type: 'apiKey',
        in: 'header',
        name: 'Authorization',
        description:
          'For accessing the API a valid JWT token must be passed in all the queries in the Authorization header.' +
          'The following syntax must be used in the Authorization header :' +
          'Bearer xxxxxx.yyyyyyy.zzzzzz'
      }
    }
  },
  apis: ['./routes/*.js']
};