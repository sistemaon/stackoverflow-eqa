const createError = require('http-errors');
const express = require('express');
const cookieParser = require('cookie-parser');
const logger = require('morgan');

const swaggerUi = require('swagger-ui-express');
const swaggerJSDoc = require('swagger-jsdoc');

// swagger js file
const swaggerOptions = require('./swagger/swagger');
const swaggerSpec = swaggerJSDoc(swaggerOptions);

// routes required
const usersRouter = require('./routes/users');

// Swagger ui options
const options = {
  explorer: false
}

const app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

// routes app to use
app.use('/api', usersRouter);

// swagger endpoint using swagger js file
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec, options));

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
