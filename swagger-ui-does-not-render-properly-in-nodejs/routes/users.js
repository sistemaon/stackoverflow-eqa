const express = require('express');
const router = express.Router();

/**
 * @swagger
 * tags:
 *   name: users
 *   description: users management
 */

/**
 * @swagger
 * path:
 *  /users:
 *    get:
 *      summary: Get all users
 *      operationId: findUsers
 *      tags: [users]
 *      responses:
 *        "200":
 *          description: An array of users
 *          schema:
 *            $ref: '#/definitions/Users'
 *          content:
 *            application/json
 */

router.get('/users', (req, res) => {
  res.status(200).json({ users: [
    { id: 1, name: 'John' },
    { id: 2, name: 'Kate' },
    { id: 3, name: 'Will' },
    { id: 4, name: 'Mary' }
  ] })
});

/**
 * @swagger
 * definitions:
 *  User:
 *    properties:
 *      id:
 *        type: integer
 *      name:
 *        type: string
 */

 /**
 * @swagger
 * definitions:
 *  Users:
 *    properties:
 *      users:
 *        type: array
 *        items:
 *          $ref: '#/definitions/User'
 */


module.exports = router;