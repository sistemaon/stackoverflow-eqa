
// /* GET users listing. */
// router.get('/', function(req, res, next) {
//   res.send('respond with a resource');
// });

const express = require('express');
const router = express.Router();

const Sequelize = require('sequelize');

const connection = new Sequelize('empdb', 'postgres', 'postgres', {
  host: 'localhost',
  port: '5432',
  dialect:  'postgres' 
});

const Emp = connection.define('emp', {
  fullName: {
    type: Sequelize.STRING
  },
  email: {
    type: Sequelize.STRING
  }
});

connection.sync().then( () => {

  // function to fetch emp
  const fetchEmp = (req, res, next) => {
    try {
      // fetchs emp info in database
      Emp.findAll().then( (emps) => {
        res.status(200).json({ emp: emps });
      });
    } catch (error) {
      res.status(400).json(error);
    }
  }

  // function to create emp
  const createEmp = (req, res, next) => {
    try {
      const objEmp = {
        fullName: req.body.fullName    || 'Emp Name',
        email:    req.body.email       || 'emp.name@emp.com'
      };
      // creates emp info in database
      Emp.create(objEmp).then( (emps) => {
        res.status(201).json({ emp: emps });
      });
    } catch (error) {
      res.status(400).json(error);
    }
  }

  /* POST emps listing. */
  router.post('/emp', createEmp);
  /* GET emps listing. */
  router.get('/emp', fetchEmp);

})

module.exports = router;
