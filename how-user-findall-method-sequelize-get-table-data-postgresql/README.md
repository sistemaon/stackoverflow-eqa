
# #2 EQA

# Running Project
- **Requirements**:
    - nodejs

**Installing**
```zsh
npm install
```

**Running**
```zsh
npm start
```

**Endpoints Access**:
```
/api/emp -> emp endpoint (only POST and GET implemented)
```

**Observations**:
Port is setup to 3000, you may change at /bin/www <br>
Or you may change port process.env.PORT when running project, as it follows:
```zsh
PORT=3003 npm start
```

# References and Resources
- #2 EQA [project code](https://gitlab.com/sistemaon/stackoverflow-eqa/-/tree/master/how-user-findall-method-sequelize-get-table-data-postgresql) and [stackoverflow question](https://stackoverflow.com/questions/63111811/how-use-findall-method-in-sequelize-to-get-table-data-from-postgresql)
    - Project name inside: _how-user-findall-method-sequelize-get-table-data-postgresql_

    - resources/references:
        - https://sequelize.org/master/manual/model-querying-basics.html
        - https://sequelize.org/master/class/lib/model.js~Model.html
        - https://sequelize.org/master/class/lib/sequelize.js~Sequelize.html#instance-constructor-constructor
        - https://sequelize.org/master/class/lib/dialects/postgres/query-interface.js~PostgresQueryInterface.html
        

### Emp POST
![emp post response](../imgs/03.png)

### Emp GET
![emp get response](../imgs/04.png)

### Emp DB Creating Table
![emp get response](../imgs/05.png)
